module monibuca

go 1.18

require (
	m7s.live/engine/v4 v4.4.1
	m7s.live/plugin/debug/v4 v4.0.0-20220506113031-34f3a736ceb2
	m7s.live/plugin/gb28181/v4 v4.0.0-20220607131724-e110acfb56d3
	m7s.live/plugin/hdl/v4 v4.0.0-20220619154106-b109dd48a6e4
	m7s.live/plugin/hls/v4 v4.0.0-20220625093111-3bf2cf54b4d3
	m7s.live/plugin/hook/v4 v4.0.0-20220510140048-d4f76fd1c415
	m7s.live/plugin/jessica/v4 v4.0.0-20220619154153-bcd0a5960ac1
	m7s.live/plugin/logrotate/v4 v4.0.0-20220506113619-1a557d4707a5
	m7s.live/plugin/preview/v4 v4.0.0-20220626084504-3a50bb4ce5f3
	m7s.live/plugin/record/v4 v4.0.0-20220625032502-232d974a1c93
	m7s.live/plugin/room/v4 v4.0.0-20220512035851-f980d4a7f6a0
	m7s.live/plugin/rtmp/v4 v4.0.0-20220619154232-ab31af1fb7d6
	m7s.live/plugin/rtsp/v4 v4.0.0-20220625121132-93d6eedff2b2
	m7s.live/plugin/snap/v4 v4.0.0-20220619154320-97a4991e775b
	m7s.live/plugin/webrtc/v4 v4.0.0-20220625121305-db54d6ee4d17
	m7s.live/plugin/webtransport/v4 v4.0.0-20220619154419-bf138bc5958b
)

require (
	github.com/adriancable/webtransport-go v0.1.0 // indirect
	github.com/aler9/gortsplib v0.0.0-20220606105133-0101ad961ccd // indirect
	github.com/cheekybits/genny v1.0.0 // indirect
	github.com/cnotch/ipchub v1.1.0 // indirect
	github.com/discoviking/fsm v0.0.0-20150126104936-f4a273feecca // indirect
	github.com/edgeware/mp4ff v0.28.0 // indirect
	github.com/fsnotify/fsnotify v1.5.4 // indirect
	github.com/ghettovoice/gosip v0.0.0-20220420085539-cf932c28a8fe // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/go-task/slim-sprig v0.0.0-20210107165309-348f09dbbbc0 // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.1.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/icza/bitio v1.1.0 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible // indirect
	github.com/lucas-clemente/quic-go v0.26.0 // indirect
	github.com/lufia/plan9stats v0.0.0-20220517141722-cf486979b281 // indirect
	github.com/marten-seemann/qpack v0.2.1 // indirect
	github.com/marten-seemann/qtls-go1-16 v0.1.5 // indirect
	github.com/marten-seemann/qtls-go1-17 v0.1.2 // indirect
	github.com/marten-seemann/qtls-go1-18 v0.1.2 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/nxadm/tail v1.4.8 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/pion/datachannel v1.5.2 // indirect
	github.com/pion/dtls/v2 v2.1.5 // indirect
	github.com/pion/ice/v2 v2.2.6 // indirect
	github.com/pion/interceptor v0.1.11 // indirect
	github.com/pion/logging v0.2.2 // indirect
	github.com/pion/mdns v0.0.5 // indirect
	github.com/pion/randutil v0.1.0 // indirect
	github.com/pion/rtcp v1.2.9 // indirect
	github.com/pion/rtp v1.7.13 // indirect
	github.com/pion/rtp/v2 v2.0.0-20220302185659-b3d10fc096b0 // indirect
	github.com/pion/sctp v1.8.2 // indirect
	github.com/pion/sdp/v3 v3.0.5 // indirect
	github.com/pion/srtp/v2 v2.0.9 // indirect
	github.com/pion/stun v0.3.5 // indirect
	github.com/pion/transport v0.13.0 // indirect
	github.com/pion/turn/v2 v2.0.8 // indirect
	github.com/pion/udp v0.1.1 // indirect
	github.com/pion/webrtc/v3 v3.1.41 // indirect
	github.com/power-devops/perfstat v0.0.0-20220216144756-c35f1ee13d7c // indirect
	github.com/q191201771/naza v0.30.2 // indirect
	github.com/quangngotan95/go-m3u8 v0.1.0 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b // indirect
	github.com/shirou/gopsutil/v3 v3.22.5 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/tevino/abool v1.2.0 // indirect
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.5.0 // indirect
	github.com/x-cray/logrus-prefixed-formatter v0.5.2 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	github.com/zhangpeihao/goamf v0.0.0-20140409082417-3ff2c19514a8 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.21.0 // indirect
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e // indirect
	golang.org/x/mod v0.6.0-dev.0.20220106191415-9b9b3d81d5e3 // indirect
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
	golang.org/x/sys v0.0.0-20220615213510-4f61da869c0c // indirect
	golang.org/x/term v0.0.0-20220526004731-065cf7ba2467 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.10 // indirect
	golang.org/x/xerrors v0.0.0-20220517211312-f3a8303e98df // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
